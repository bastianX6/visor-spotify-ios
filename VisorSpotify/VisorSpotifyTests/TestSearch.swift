//
//  TestSearch.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit
import XCTest

class TestSearch: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSearchArtist() {
        
        
        var artista = "Los Bunkers";
        
        var url = SpotifyURL.SearchArtist + "&q="+artista.lowercased().getUrlEncodedString()!
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            
            println("Response: "+responseString)
            
            var json = JSON(stringJSON: responseString)
            
            if let jsonElement = json["artists"]{
                var object : PagingObject = JSONSpotify.getPagingObject(jsonElement, clase : ArtistFull.self)
                
                println("Object en json: "+object.toJsonString())
            }
            
        }
        else{
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }
    
    func testSearchArtistEmpty() {
        
        
        var artista = "";
        
        var url = SpotifyURL.SearchArtist + "&q="+artista.lowercased().getUrlEncodedString()!
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            
            XCTFail("Error al ejecutar resquest a spotify - valor debe ser nulo")
            
        }
        else{
            println("Valor nulo, porque codigo de request es != 200")
        }
    }
    
    func testSearchArtistStringRaro() {
        
        
        var artista = "@#";
        
        var url = SpotifyURL.SearchArtist + "&q="+artista.lowercased().getUrlEncodedString()!
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            
            println("response string: "+responseString)
            
            
        }
        else{
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }
    
    func testSearchAlbums(){
        
        var url  = "https://api.spotify.com/v1/artists/1vCWHaC5f2uS3yhpwWbIA6/albums?album_type=single&limit=2"
        
        if let responseString = SpotifyRequest.GetResponseString(url)
        {
            println("response string: "+responseString)
            var json = JSON(stringJSON: responseString)
            
            var object : PagingObject = JSONSpotify.getPagingObject(json, clase: AlbumObjectSimple.self)
            
            println("Paging Object: "+object.toJsonString())
        }
        else
        {
            XCTFail("Error al realizar request a Spotify")
        }
    }
    
    func testSearchArtistAlbums() {
        
        
        var artista = "Los Bunkers";
        
        var url = SpotifyURL.SearchArtist + "&q="+artista.lowercased().getUrlEncodedString()!
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            
            //println("Response: "+responseString)
            
            var json = JSON(stringJSON: responseString)
            
            if let jsonElement = json["artists"]{
                var object : PagingObject = JSONSpotify.getPagingObject(jsonElement, clase : ArtistFull.self)
                
                for item in object.items
                {
                    var artist = item as! ArtistFull
                    var artistID = artist.id
                    
                    println("\n-----\nAritsta: "+artist.name!)
                    //println("Albumes: \n")
                    
                    var urlAlbums = SpotifyURL.BASE_URL + "/v1/artists/\(artistID!)/albums"
                    
                    if let responseAlbums = SpotifyRequest.GetResponseString(urlAlbums){
                        
                        println("response albums: "+responseAlbums)
                        
                        var jsonAlbums = JSON(stringJSON: responseAlbums)
                        var objectAlbums : PagingObject = JSONSpotify.getPagingObject(jsonAlbums, clase: AlbumObjectSimple.self)
                        
                        for itemAlbum in objectAlbums.items
                        {
                            var album = itemAlbum as! AlbumObjectSimple
                            //println("Nombre album: "+album.name!+" | tipo: "+album.type!)
                        }
                        
                        
                    }
                    else
                    {
                       XCTFail("Error al ejecutar resquest a spotify - albums")
                    }
                    
                }
            }
            
        }
        else{
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }
    
    func testSearchArtistTopTracks(){
        var artistID = "1vCWHaC5f2uS3yhpwWbIA6"
        var country = "CL"
        
        if let array = SpotifyRequest.SearchArtistTopTracks(artistID, country : country)
        {
            println("tracks: \n\n")
            for element in array
            {
                println(element.toJsonString()+"\n\n")
            }
        }
        else
        {
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }
    
    func testSearchAlbumTracks(){
        
        var albumID = "6akEvsycLGftJxYudPjmqK"
        
        if let object = SpotifyRequest.SearchAlbumTracks(albumID){
            
            println("Object: "+object.toJsonString())
        }
        else
        {
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }
    
    func testSearchArtist2()
    {
        var artist = "soda"
        
        if let object = SpotifyRequest.SearchArtists(artist)
        {
            println("Object: "+object.toJsonString())
        }
        else
        {
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }
    
    func testSearchArtistAlbums2(){
        var artistID = "1vCWHaC5f2uS3yhpwWbIA6"
        
        if let object = SpotifyRequest.SearchArtistAlbums(artistID)
        {
            println("Object: "+object.toJsonString())
        }
        else
        {
            XCTFail("Error al ejecutar resquest a spotify")
        }
    }

}
