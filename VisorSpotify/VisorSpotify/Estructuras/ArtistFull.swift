//
//  ArtistFull.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class ArtistFull: Serializable {
   
    var external_urls : ExternalURLObject?
    var followers : Followers?
    var genres : Array<String> = []
    var href : String?
    var id : String?
    var images : Array<ImageObject> = []
    var name : String?
    var popularity : NSNumber?
    var type : String?
    var uri : String?
}
