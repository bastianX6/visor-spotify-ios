//
//  AlbumObjectSimple.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class AlbumObjectSimple: Serializable {
   
    var album_type : String?
    var available_markets : Array<String> = []
    var external_urls : ExternalURLObject?
    var href : String?
    var id : String?
    var images : Array<ImageObject> = []
    var name : String?
    var type : String?
    var uri : String?
    
}
