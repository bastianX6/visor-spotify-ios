//
//  PagingObject.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class PagingObject: Serializable {
   
    var href : String?
    var items : Array<Serializable> = []
    var limit : NSNumber?
    var next : String?
    var offset : NSNumber?
    var previous : String?
    var total : NSNumber?
}
