//
//  ImageObject.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class ImageObject: Serializable {
    
    var height : NSNumber?
    var width : NSNumber?
    var url : String?
   
}
