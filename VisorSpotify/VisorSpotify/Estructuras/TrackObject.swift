//
//  TrackObjectFull.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class TrackObject: Serializable {
   
    var album : AlbumObjectSimple?
    var artists : Array<ArtistSimple> = []
    var available_markets : Array<String> = []
    var disc_number : NSNumber?
    var duration_ms : NSNumber?
    var name : String?
    var popularity  : NSNumber?
    var track_number : NSNumber?
    var type : String?
    var uri : String?
    
}
