//
//  ArtistSimple.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class ArtistSimple: Serializable {
   
    var external_urls : ExternalURLObject?
    var href : String?
    var id : String?
    var images : Array<ImageObject> = []
    var name : String?
    var type : String?
    var uri : String?
}
