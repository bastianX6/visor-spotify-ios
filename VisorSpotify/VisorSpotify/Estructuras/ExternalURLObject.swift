//
//  ExternalURLObject.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class ExternalURLObject: Serializable {
   
    var dict : Dictionary<String, String>?
}
