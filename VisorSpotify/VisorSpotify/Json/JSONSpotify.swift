//
//  JSONSpotify.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import Foundation
import SwiftyJSON


class JSONSpotify{
    
    
    class func getPagingObject(_ json : JSON, clase : AnyClass) -> PagingObject{
        
        let object = PagingObject()
        
        
        object.href = JSONUtilities.getStringValue("href", json: json)
        object.limit = JSONUtilities.getLongValue("limit", json: json)
        object.next = JSONUtilities.getStringValue("next", json: json)
        object.offset = JSONUtilities.getLongValue("offset", json: json)
        object.previous = JSONUtilities.getStringValue("previous", json: json)
        object.total = JSONUtilities.getLongValue("total", json: json)
        
        
        if let array = json["items"].array{

            for element in array{
                if clase is ArtistFull.Type
                {
                   object.items.append(JSONSpotify.getArtistFull(element))
                }
                else if clase is AlbumObjectSimple.Type
                {
                    object.items.append(JSONSpotify.getAlbumObjectSimple(element))
                }
                else if clase is TrackObject.Type
                {
                    object.items.append(JSONSpotify.getTrackObject(element))
                }
            }
            
            
        }
        return object
        
    }
    
    
    class func getArtistFull(_ json : JSON) -> ArtistFull{
        
        let artist = ArtistFull()
        
        artist.external_urls = self.getExternalURLObject("external_urls", json: json)
        artist.followers = self.getFollowers("followers", json: json)

        if let array = JSONUtilities.getArrayValue("genres", json: json){
            
            for element in array{
                let genre = element.stringValue
                artist.genres.append(genre)
            }
        }
        
        artist.href = JSONUtilities.getStringValue("href", json: json)
        artist.id = JSONUtilities.getStringValue("id", json: json)
        
        if let array = JSONUtilities.getArrayValue("images", json: json){
            
            for element in array{
                artist.images.append(self.getImageObject(element))
            }
        }
        
        artist.name = JSONUtilities.getStringValue("name", json: json)
        artist.popularity = JSONUtilities.getLongValue("popularity", json: json)
        artist.type = JSONUtilities.getStringValue("type", json: json)
        artist.uri = JSONUtilities.getStringValue("uri", json: json)
        
        return artist
    }
    
    class func getArtistSimple(_ json : JSON) -> ArtistSimple{
        
        let artist = ArtistSimple()
        
        artist.external_urls = self.getExternalURLObject("external_urls", json: json)
        artist.href = JSONUtilities.getStringValue("href", json: json)
        artist.id = JSONUtilities.getStringValue("id", json: json)
        
        if let array = JSONUtilities.getArrayValue("images", json: json){
            
            for element in array{
                artist.images.append(self.getImageObject(element))
            }
        }
        
        artist.name = JSONUtilities.getStringValue("name", json: json)
        artist.type = JSONUtilities.getStringValue("type", json: json)
        artist.uri = JSONUtilities.getStringValue("uri", json: json)
        
        return artist
    }

    
    
    class func getAlbumObjectSimple(_ json : JSON) -> AlbumObjectSimple{
        
        var album = AlbumObjectSimple()
        
        album.external_urls = self.getExternalURLObject("external_urls", json: json)
        
        if let array = JSONUtilities.getArrayValue("available_markets", json: json){
            
            for element in array{
                
                let market = element.stringValue
                album.available_markets.append(market)
            }
        }
        
        album.album_type = JSONUtilities.getStringValue("album_type", json: json)
        album.href = JSONUtilities.getStringValue("href", json: json)
        album.id = JSONUtilities.getStringValue("id", json: json)
        
        if let array = JSONUtilities.getArrayValue("images", json: json){
            
            for element in array{
                album.images.append(self.getImageObject(element))
            }
        }
        
        album.name = JSONUtilities.getStringValue("name", json: json)
        album.type = JSONUtilities.getStringValue("type", json: json)
        album.uri = JSONUtilities.getStringValue("uri", json: json)
        
        return album
    }

    
    class func getExternalURLObject(_ clave : String, json : JSON) -> ExternalURLObject?{
        
        let jsonElement = json[clave]
        
        let object = ExternalURLObject()
        object.dict = JSONUtilities.getStringDictionaryValue(clave, json: json)
        
        return object
        
        return nil
        
        
    }
    
    class func getFollowers(_ clave : String, json : JSON) -> Followers?{
        
        let jsonElement = json[clave]
        let followers = Followers()
        
        followers.href = JSONUtilities.getStringValue("href", json: jsonElement)
        followers.total = JSONUtilities.getLongValue("total", json: jsonElement)
        
        return followers
        
    }
    
    class func getImageObject(_ json : JSON) -> ImageObject{
        
        let image  = ImageObject()
        
        image.width = JSONUtilities.getLongValue("width", json: json)
        image.height = JSONUtilities.getLongValue("height", json: json)
        image.url = JSONUtilities.getStringValue("url", json: json)
        
        return image
    }
    
    class func getTrackObject(_ json : JSON) -> TrackObject{
        
        var track = TrackObject()
        
        let jsonElement = json["album"]
        track.album = self.getAlbumObjectSimple(jsonElement)
        
        if let jsonArray = json["artists"].array
        {
            for element in jsonArray
            {
                track.artists.append(self.getArtistSimple(element))
            }
        }
        
        if let array = JSONUtilities.getArrayValue("available_markets", json: json){
            
            for element in array{
                
                let market = element.stringValue
                track.available_markets.append(market)
            }
        }
        
        track.disc_number = JSONUtilities.getLongValue("disc_number", json: json)
        track.duration_ms = JSONUtilities.getLongValue("duration_ms", json: json)
        track.name = JSONUtilities.getStringValue("name", json: json)
        track.popularity = JSONUtilities.getLongValue("popularity", json: json)
        track.track_number = JSONUtilities.getLongValue("track_number", json: json)
        track.type = JSONUtilities.getStringValue("type", json: json)
        track.uri = JSONUtilities.getStringValue("uri", json: json)
        
        return track
    }

}
