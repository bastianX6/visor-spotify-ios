//
//  JSONUtilities.swift
//  PocketLawyer
//
//  Created by Bastián Véliz Vega on 28-12-14.
//  Copyright (c) 2014 Aionsoft LTDA. All rights reserved.
//

import Foundation
import SwiftyJSON

open class JSONUtilities{
    
    class func getLongValue(_ clave : String, json : JSON) -> NSNumber?
    {
        return json[clave].numberValue
    }
    
    class func getStringValue(_ clave : String, json : JSON) -> String?
    {
        return json[clave].stringValue
    }
    
    class func getBoolValue(_ clave : String, json : JSON) -> Bool
    {
        return json[clave].boolValue
    }
    
    class func getArrayValue(_ clave : String, json : JSON) -> Array<JSON>?
    {
        return json[clave].arrayValue
    }
    
    class func getStringDictionaryValue(_ clave : String, json : JSON) -> Dictionary<String,String>?
    {
        let dictionary = json[clave].dictionaryValue
        
        let mutableDict = NSMutableDictionary()
        
        for (key,value) in dictionary{
            let stringValue = value.stringValue
            mutableDict.setValue(stringValue, forKey: key)
            
        }
        
        return NSDictionary(dictionary: mutableDict) as? Dictionary<String, String>
    }
    
    class func stringToDate(_ fechaString :String?) -> Date?
    {
        
        if (fechaString == nil)
        {
            return nil
        }
        
        
        let format = "MMM d, yyyy hh:mm:ss a"
        let formatter : DateFormatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = format
        return formatter.date(from: fechaString!)
    }

}
