//
//  ViewController.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let bundle = Bundle.main
        let path = bundle.path(forResource: "descripcion", ofType: "html")
        self.webView.loadRequest(URLRequest(url: URL(fileURLWithPath: path!)))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionComenzar(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ControllerBusquedaArtista") as! ControllerBusquedaArtista
        
        let controllerMenu = storyboard.instantiateViewController(withIdentifier: "ControllerMenu") as! ControllerMenu
        let navController = self.slideMenuController()?.leftViewController as? UINavigationController
        
        navController?.setViewControllers([controllerMenu], animated: false)
        self.navigationController?.setViewControllers([controller], animated: true)
    }

}

