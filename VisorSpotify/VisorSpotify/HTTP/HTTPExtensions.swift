//
//  HTTPExtensions.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import Foundation

public extension String{
    
    func getUrlEncodedString() -> String?
    {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
    func getUrlDecodedString() -> String?
    {
        return self.removingPercentEncoding
    }
}
