//
//  DownloadHelper.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit
import XCGLogger

class DownloadHelper: NSObject, URLSessionDownloadDelegate {
   
    let log = XCGLogger.default
    var link : String
    var fileType : String
    var fileName : String
    var folder : String
    var session : Foundation.URLSession!
    
    init(link : String, fileType : String) {
        self.link = link
        self.fileName = "\(link.hashValue)"
        self.fileType = fileType
        self.folder = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
    }
    
    func descargarArchivo()
    {

        let fileManager = FileManager()
        
        let path = self.folder+"/"+self.fileName+"."+self.fileType
        
        let urlFilePath = URL(fileURLWithPath: path)

        do {
            try fileManager.createDirectory(atPath: self.folder, withIntermediateDirectories: true, attributes: nil)
        } catch {
            
        }
        
        if let url = URL(string: self.link) {
            if let data = try? Data(contentsOf: url){
                try? data.write(to: URL(fileURLWithPath: path), options: [])
            }
        }
        
        /*
        //Inicializar URLSession para la descarga
        var sessionConfiguration:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.HTTPMaximumConnectionsPerHost = 5
        self.session = NSURLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        
        //Iniciar task para realizar descarga
        var downloadTask:NSURLSessionDownloadTask = self.session!.downloadTaskWithURL(NSURL(string: self.link)!)
        downloadTask.resume()
        */
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        
        if exp != NSURLSessionTransferSizeUnknown
        {
            //println("downloaded \((100*writ)/exp)%")
        }
        else
        {
            //println("downloaded \(writ) bytes")
        }
        
        
        
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        if(error != nil) {
            log.error("Error al realizar descarga: \(error!.localizedDescription)");
        }
        else {
            //log.info("Descarga finalizada correctamente")
        }
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        var error : NSError?
        var fileManager = FileManager()
        
        var path = self.folder+"/"+self.fileName+"."+self.fileType
        
        var urlFilePath = URL(fileURLWithPath: path)
        
        do {
            try fileManager.createDirectory(atPath: self.folder, withIntermediateDirectories: true, attributes: nil)
        } catch {
            
        }
        
        if fileManager.fileExists(atPath: path) {
            do {
                try fileManager.removeItem(at: urlFilePath)
            } catch {
                
            }
        }
        
        /*copy from the temp location to the final location*/
        
        do {
            try fileManager.copyItem(at: location, to: urlFilePath)
        } catch {
            log.error("Error al crear archivo: \(error)")
        }
        
    }
    
    class func existeArchivo(_ path : String) -> Bool
    {
        let fileManager = FileManager()
        return fileManager.fileExists(atPath: path)
    }
    
    class func getFilePath(_ link : String, fileType : String) -> String
    {
        let fileName = "\(link.hashValue)"
        let folder = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
        let path = folder+"/"+fileName+"."+fileType
        
        return path
    }
    
    class func getImagePath(_ link : String) -> String
    {
        let path = DownloadHelper.getFilePath(link, fileType: "jpg")
        if !DownloadHelper.existeArchivo(path)
        {
            return ""
        }
        
        return path
    }
}
