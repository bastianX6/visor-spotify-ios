//
//  SpotifyURL.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import Foundation

public struct SpotifyURL{
    
    static let BASE_URL = "https://api.spotify.com"
    static let SearchArtist = BASE_URL+"/v1/search?type=artist"
    static let SearchAlbum = BASE_URL+"/v1/search?type=album"
    
}