//
//  SpotifyRequest.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 24-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import Foundation
import XCGLogger
import SwiftyJSON

class SpotifyRequest{
    
    class func GetResponseString(_ url : String) -> String?
    {
        let log = XCGLogger.default
        
        let theUrl = URL(string: url)
        
        /*Generar HTTP request*/
        let request = NSMutableURLRequest(url : theUrl!)
        
        request.httpMethod = "GET"
        request.timeoutInterval = 15

        var response: URLResponse? = nil
        
        do {
            let requestData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning:  &response)
            
            if let resp = response
            {
                if (resp as? HTTPURLResponse)?.statusCode != 200
                {
                    log.error("error Response =\(resp as? HTTPURLResponse)")
                    return nil
                }
            }
            
            if let html = NSString(data: requestData, encoding: String.Encoding.utf8.rawValue) as? String
            {
                //log.debug("HTML: \(html)")
                return html
            }
            
            if let html = NSString(data: requestData, encoding: String.Encoding.isoLatin1.rawValue) as? String
            {
                //log.debug("HTML: \(html)")
                return html
            }
            
            
        } catch  {
            log.error("error! : \(error)")
            return nil
        }
        
        return nil
        
    }
    
    class func SearchArtists(_ artista : String) -> PagingObject?{
        
        let url = SpotifyURL.SearchArtist + "&q="+artista.lowercased().getUrlEncodedString()!
        let log = XCGLogger.default
        if let responseString = SpotifyRequest.GetResponseString(url){
            //println("Response: "+responseString)
            
            do {
                let dict = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
                let json = JSON(dict)
                let jsonElement = json["artists"]
                let object : PagingObject = JSONSpotify.getPagingObject(jsonElement, clase : ArtistFull.self)
                return object
                
            } catch  {
                log.debug("error!: \(error)")
                return nil
            }
        }
        return nil

    }
    
    class func SearchAlbums(_ albumName : String) -> PagingObject?{
        
        let url = SpotifyURL.SearchAlbum + "&q="+albumName.lowercased().getUrlEncodedString()!
        let log = XCGLogger.default
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            //println("Response: "+responseString)
            
            do {
                let dict = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
                let json = JSON(dict)
                let jsonElement = json["albums"]
                let object : PagingObject = JSONSpotify.getPagingObject(jsonElement, clase : AlbumObjectSimple.self)
                return object
                
            } catch  {
                log.debug("error!: \(error)")
                return nil
            }
            
        }
        return nil
        
    }
    
    class func SearchArtistAlbums(_ artistID : String) -> PagingObject?{
        
        let url = SpotifyURL.BASE_URL + "/v1/artists/\(artistID)/albums"
        let log = XCGLogger.default
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            //println("Response: "+responseString)
            do {
                let dict = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
                let json = JSON(dict)
                let object : PagingObject = JSONSpotify.getPagingObject(json, clase : AlbumObjectSimple.self)
                return object
                
            } catch  {
                log.debug("error!: \(error)")
                return nil
            }
        }
        return nil
        
    }
    
    class func SearchAlbumTracks(_ albumID : String) -> PagingObject?{
        
        let url = SpotifyURL.BASE_URL + "/v1/albums/\(albumID)/tracks"
        let log = XCGLogger.default
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            //println("Response: "+responseString)
            do {
                let dict = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
                let json = JSON(dict)
                let object : PagingObject = JSONSpotify.getPagingObject(json, clase : TrackObject.self)
                return object
                
            } catch  {
                log.debug("error!: \(error)")
                return nil
            }
        }
        return nil
        
    }
    
    class func SearchArtistTopTracks(_ artistID : String, country : String) -> Array<TrackObject>?{
        
        let url = SpotifyURL.BASE_URL + "/v1/artists/\(artistID)/top-tracks?country=\(country)"
        let log = XCGLogger.default
        
        if let responseString = SpotifyRequest.GetResponseString(url){
            //println("Response: "+responseString)
            do {
                let dict = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
                let json = JSON(dict)
                if let jsonArray = json["tracks"].array{
                    
                    var trackArray : Array<TrackObject> = []
                    for element in jsonArray
                    {
                        trackArray.append(JSONSpotify.getTrackObject(element))
                    }
                    
                    return trackArray
                }
                
            } catch  {
                log.debug("error!: \(error)")
                return nil
            }
        }
        return nil
        
    }

    class func descargarImagenes(_ images : Array<ImageObject>)
    {
        if images.count > 0
        {
            let image = images.first!
            
            if let url = image.url
            {
                let path = DownloadHelper.getFilePath(url, fileType: "jpg")
                
                if !DownloadHelper.existeArchivo(path){
                    let downloadHelper = DownloadHelper(link: url, fileType: "jpg")
                    downloadHelper.descargarArchivo()
                }
            }
        }
    }
    
    
}
