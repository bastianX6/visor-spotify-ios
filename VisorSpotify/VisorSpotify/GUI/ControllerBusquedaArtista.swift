//
//  ControllerBusquedaArtista.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit
import MBProgressHUD
import SlideMenuControllerSwift

class ControllerBusquedaArtista: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {

    var searchController : UISearchController = UISearchController()
    
    var listaElementos : NSMutableArray = NSMutableArray()
    var app = UIApplication.shared
    
    var hud : MBProgressHUD?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hud = MBProgressHUD(for: UIApplication.shared.keyWindow!)
        self.slideMenuController()?.addLeftGestures()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

        self.searchController = ({
            // Two setups provided below:
            
            // Setup One: This setup present the results in the current view.
            let controller = UISearchController(searchResultsController: nil)
            
            controller.searchResultsUpdater = self
            controller.hidesNavigationBarDuringPresentation = false
            controller.dimsBackgroundDuringPresentation = false
            
            controller.searchBar.searchBarStyle = UISearchBarStyle.prominent
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //UISearchResultsUpdating protocol
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchText = self.searchController.searchBar.text
        let queue = OperationQueue()
        
        if searchText != ""
        {
            
            
            self.hud?.label.text = "Buscando"
            self.hud?.show(animated: true)
            
            app.isNetworkActivityIndicatorVisible = true
            queue.addOperation(){
                
                if let pagingObject = SpotifyRequest.SearchArtists(searchText!)
                {
                    if pagingObject.items.count > 0{
                        let arregloArtistas = pagingObject.items as! Array<ArtistFull>
                        self.listaElementos.removeAllObjects()
                        self.listaElementos.addObjects(from: arregloArtistas)
                        
                        for artist in arregloArtistas
                        {
                            SpotifyRequest.descargarImagenes(artist.images)
                        }
                    }
                    else
                    {
                        self.listaElementos.removeAllObjects()
                    }
                }
                else
                {
                    self.listaElementos.removeAllObjects()
                }
                
                
                //actualizar GUI
                OperationQueue.main.addOperation(){
                    
                    self.tableView.separatorStyle = self.listaElementos.count > 0 ? UITableViewCellSeparatorStyle.singleLine : UITableViewCellSeparatorStyle.none
                    
                    self.tableView.reloadData()
                    self.app.isNetworkActivityIndicatorVisible = false
                    self.hud?.hide(animated: true)
                }
            }
        }
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        OperationQueue.main.addOperation(){
            self.listaElementos.removeAllObjects()
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            self.tableView.reloadData()
            self.app.isNetworkActivityIndicatorVisible = false
            self.hud?.hide(animated: true)
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listaElementos.count > 0 ? listaElementos.count : 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellMensaje = Bundle.main.loadNibNamed("CellMensajeBusqueda", owner: self, options: nil)?[0] as! CellMensajeBusqueda
        
        if listaElementos.count > 0
        {
            let cell = Bundle.main.loadNibNamed("CellBusqueda", owner: self, options: nil)?[0] as! CellBusqueda
            
            let artista = listaElementos[indexPath.row] as! ArtistFull
            var imagePath = ""
            
            let seguidores = artista.followers?.total != nil ? "\(artista.followers!.total!) seguidores" : "Sin seguidores"
            
            if artista.images.count > 0
            {
                if let url = artista.images.first?.url
                {
                    imagePath = DownloadHelper.getImagePath(url)
                }
                
            }
            
            cell.setValores(artista.name, subtitulo: seguidores, imagePath: imagePath)
            return cell
        }

        return cellMensaje
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.listaElementos.count > 0
        {
            let queue = OperationQueue()
            let artista = listaElementos[indexPath.row] as! ArtistFull
            let storyBoard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerArtista") as! ControllerArtista
            
            controller.artista = artista
            self.hud?.label.text = "Buscando"
            self.hud?.show(animated: true)
            app.isNetworkActivityIndicatorVisible = true
            
            queue.addOperation(){
                
                if let idArtista = artista.id
                {
                    if let tracks = SpotifyRequest.SearchArtistTopTracks(idArtista, country: "CL")
                    {
                        controller.listaCanciones = tracks
                    }
                    
                    if let object = SpotifyRequest.SearchArtistAlbums(idArtista)
                    {
                        let albums = object.items as! Array<AlbumObjectSimple>
                        controller.listaAlbumes = albums
                    }
                }
                
                let navControllerLeft = self.slideMenuController()?.leftViewController as? UINavigationController
                let controllerMenu = navControllerLeft?.viewControllers.last as? ControllerMenu
                controllerMenu?.agregarAlHistorial(artista)
                
                OperationQueue.main.addOperation(){
                    self.app.isNetworkActivityIndicatorVisible = false
                    self.hud?.hide(animated: true)
                    self.searchController.isActive = false
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.listaElementos.count > 0
        {
            return 60
        }
        
        return 190
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.darkGray
        cell.textLabel?.textColor = UIColor.white
    }
    
    
    @IBAction func openLeft(_ sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }

    
}
