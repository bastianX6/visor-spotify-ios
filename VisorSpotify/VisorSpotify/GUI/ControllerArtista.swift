//
//  ControllerArtista.swift
//  VisorSpotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit
import MBProgressHUD

class ControllerArtista: UITableViewController {

    var secciones = ["Artista","Canciones populares","Álbumes"]
    var listaAlbumes : Array<AlbumObjectSimple> = []
    var listaCanciones : Array<TrackObject> = []
    var artista : ArtistFull!
    var app = UIApplication.shared
    var hud : MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = artista.name
        self.descargarCaratulasAlbumes()
        self.hud = MBProgressHUD(for: UIApplication.shared.keyWindow!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1
        {
            return listaCanciones.count
        }
        else if section == 2
        {
            return listaAlbumes.count
        }
        
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellDefault = UITableViewCell()
        let section = indexPath.section
        let row = indexPath.row
        
        if section == 0
        {
            let cell = Bundle.main.loadNibNamed("CellArtista", owner: self, options: nil)?[0] as! CellArtista
            let seguidores = artista.followers?.total != nil ? "\(artista.followers!.total!)" : "Sin"
            var path = ""
            
            if let url = artista.images.first?.url
            {
                path = DownloadHelper.getImagePath(url)
            }
            cell.setValores(artista.name, seguidores: seguidores+" seguidores", imagePath: path)
            return cell
        }
        else if section == 1 && listaCanciones.count > 0
        {
            let cell = Bundle.main.loadNibNamed("CellBusqueda", owner: self, options: nil)?[0] as! CellBusqueda
            let track = listaCanciones[row]
            
            cell.setValores(track.name, subtitulo: track.album?.name, imagePath: "")
            return cell
        }
        else if section == 2 && listaAlbumes.count > 0
        {
            let cell = Bundle.main.loadNibNamed("CellBusqueda", owner: self, options: nil)?[0] as! CellBusqueda
            let album = listaAlbumes[row]
            var path = ""
            
            if let url = album.images.first?.url
            {
                path = DownloadHelper.getImagePath(url)
            }
            
            
            cell.setValores(album.name, subtitulo: artista.name, imagePath: path)
            return cell
        }
        

        return cellDefault
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2
        {
            let queue = OperationQueue()
            let album = listaAlbumes[indexPath.row]
            let storyBoard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerAlbum") as! ControllerAlbum
            
            controller.album = album
            self.hud?.label.text = "Buscando"
            self.hud?.show(animated: true)
            app.isNetworkActivityIndicatorVisible = true
            
            queue.addOperation(){
                
                if let object = SpotifyRequest.SearchAlbumTracks(album.id!)
                {
                    controller.listaCanciones = object.items as! Array<TrackObject>
                }
                
                OperationQueue.main.addOperation(){
                    self.app.isNetworkActivityIndicatorVisible = false
                    self.hud?.hide(animated: true)
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            
            
        }
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0
        {
            return nil
        }
        
        return secciones[section]
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        
        switch section
        {
        case 0:
            return 225
        case 1:
            return 60
        case 2:
            return 60
        default:
            return 40
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.darkGray
        cell.textLabel?.textColor = UIColor.white
    }
    
    func descargarCaratulasAlbumes(){
        
        let queue = OperationQueue()
        self.app.isNetworkActivityIndicatorVisible = true
        queue.addOperation(){
            for album in self.listaAlbumes
            {
                SpotifyRequest.descargarImagenes(album.images)
            }
            OperationQueue.main.addOperation(){
                self.tableView.reloadData()
                self.app.isNetworkActivityIndicatorVisible = false
            }
            
        }
    }
}
