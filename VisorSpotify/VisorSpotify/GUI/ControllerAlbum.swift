//
//  ControllerAlbum.swift
//  VisorSpotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class ControllerAlbum: UITableViewController {
    
    var secciones = ["Album","Lista de canciones"]
    var listaCanciones : Array<TrackObject> = []
    //var artista : ArtistFull!
    var album : AlbumObjectSimple!
    var app = UIApplication.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = album.name
        self.descargarCaratulasAlbumes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1
        {
            return listaCanciones.count
        }
        
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellDefault = UITableViewCell()
        var section = indexPath.section
        var row = indexPath.row
        
        if section == 0
        {
            var cell = Bundle.main.loadNibNamed("CellArtista", owner: self, options: nil)?[0] as! CellArtista
            
            var artistas = NSMutableString()
            
            if let listaArtistas = listaCanciones.first?.artists
            {
                for i in 0 ..< listaArtistas.count
                {
                    if let nombreArtista = listaArtistas[i].name
                    {
                        artistas.append(nombreArtista)
                        
                        if i != listaArtistas.count - 1
                        {
                            artistas.append(", ")
                        }
                    }
                }
            }
            
            var path = ""
            
            if let url = album.images.first?.url
            {
                path = DownloadHelper.getImagePath(url)
            }
            cell.setValores(album.name!, seguidores: artistas as String, imagePath: path)
            
            return cell
        }
        else if section == 1 && listaCanciones.count > 0
        {
            var cell = Bundle.main.loadNibNamed("CellBusqueda", owner: self, options: nil)?[0] as! CellBusqueda
            var track = listaCanciones[row]
            
            var path = ""
            
            if let url = album.images.first?.url
            {
                path = DownloadHelper.getImagePath(url)
            }
            
            cell.setValores(track.name, subtitulo: track.album?.name, imagePath: path)
            return cell
        }
        return cellDefault
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0
        {
            return nil
        }
        
        return secciones[section]
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        
        switch section
        {
        case 0:
            return 225
        case 1:
            return 60
        default:
            return 40
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.darkGray
        cell.textLabel?.textColor = UIColor.white
    }
    
    func descargarCaratulasAlbumes(){
        
        var queue = OperationQueue()
        self.app.isNetworkActivityIndicatorVisible = true
        queue.addOperation(){
            SpotifyRequest.descargarImagenes(self.album.images)
            OperationQueue.main.addOperation(){
                self.tableView.reloadData()
                self.app.isNetworkActivityIndicatorVisible = false
            }
            
        }
    }
}
