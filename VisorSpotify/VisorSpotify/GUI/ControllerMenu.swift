//
//  ControllerMenu.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit
import MBProgressHUD

class ControllerMenu: UITableViewController {

    
    var listaHistorial = NSMutableArray()
    var secciones = ["Encontrar","Historial de búsquedas"]
    var app = UIApplication.shared
    let hud = MBProgressHUD(for: UIApplication.shared.keyWindow!)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellEspecial = Bundle.main.loadNibNamed("CellBusqueda", owner: self, options: nil)?[0] as! CellBusqueda
        
        let section = indexPath.section
        let row = indexPath.row
        
        
        if section == 0
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            if row == 0
            {
                cell.textLabel?.text = "Encontrar artistas"
            }
            else if row == 1
            {
                cell.textLabel?.text = "Encontrar álbumes"
            }
            return cell
        }
        else if section == 1
        {
            if listaHistorial.count > 0
            {
                let object = self.listaHistorial[row] as! Serializable
                
                
                if object is AlbumObjectSimple
                {
                    let album = object as! AlbumObjectSimple
                    var imagePath = ""
                    
                    
                    if album.images.count > 0
                    {
                        if let url = album.images.first?.url
                        {
                            imagePath = DownloadHelper.getImagePath(url)
                        }
                        
                    }
                    
                    cellEspecial.setValores(album.name, subtitulo: "Álbum", imagePath: imagePath)
                    return cellEspecial
                }
                    
                else if object is ArtistFull
                {
                    let artista = object as! ArtistFull
                    var imagePath = ""
                    
                    if artista.images.count > 0
                    {
                        if let url = artista.images.first?.url
                        {
                            imagePath = DownloadHelper.getImagePath(url)
                        }
                        
                    }
                    
                    cellEspecial.setValores(artista.name, subtitulo: "Artista", imagePath: imagePath)
                    return cellEspecial
                }
                
                
            }
            else
            {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                cell.textLabel?.text = "No existen búsquedas"
                return cell
            }
        }
        
        
        return cellEspecial
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        let section = indexPath.section
        let storyBoard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
        
        if section == 0
        {
            if row == 0
            {
                let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerBusquedaArtista") as! ControllerBusquedaArtista
                let navController = self.slideMenuController()?.mainViewController as? UINavigationController
                navController?.setViewControllers([controller], animated: true)
                self.slideMenuController()?.closeLeft()
                
            }
            else if row == 1
            {
                let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerBusquedaAlbum") as! ControllerBusquedaAlbum
                let navController = self.slideMenuController()?.mainViewController as? UINavigationController
                navController?.setViewControllers([controller], animated: true)
                self.slideMenuController()?.closeLeft()
            }
        }
        
        if section == 1 && listaHistorial.count > 0
        {
            let object = self.listaHistorial[row] as! Serializable
            let storyBoard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
            let queue = OperationQueue()
            
            let navControllerLeft = self.slideMenuController()?.leftViewController as? UINavigationController
            let navControllerCenter = self.slideMenuController()?.mainViewController as? UINavigationController
            let controllerCenter = navControllerLeft?.viewControllers.last as? UITableViewController

            if object is AlbumObjectSimple
            {
                let album = listaHistorial[row] as! AlbumObjectSimple
                let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerAlbum") as! ControllerAlbum
                
                controller.album = album
                self.hud?.label.text = "Buscando"
                self.hud?.show(animated: true)
                app.isNetworkActivityIndicatorVisible = true
                
                queue.addOperation(){
                    
                    if let object = SpotifyRequest.SearchAlbumTracks(album.id!)
                    {
                        controller.listaCanciones = object.items as! Array<TrackObject>
                    }
                    
                    
                    OperationQueue.main.addOperation(){
                        self.app.isNetworkActivityIndicatorVisible = false
                        self.hud?.hide(animated: true)
                        
                        if controllerCenter is ControllerBusquedaAlbum
                        {
                            (controllerCenter as! ControllerBusquedaAlbum).searchController.isActive = false
                        }
                        else if controllerCenter is ControllerBusquedaArtista
                        {
                            (controllerCenter as! ControllerBusquedaArtista).searchController.isActive = false
                        }
                        
                        navControllerCenter?.pushViewController(controller, animated: true)
                        self.slideMenuController()?.closeLeft()
                    }
                }

            }
            else if object is ArtistFull
            {
                
                let artista = listaHistorial[indexPath.row] as! ArtistFull
                let storyBoard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerArtista") as! ControllerArtista
                
                controller.artista = artista
                self.hud?.label.text = "Buscando"
                self.hud?.show(animated: true)
                app.isNetworkActivityIndicatorVisible = true
                
                queue.addOperation(){
                    
                    if let idArtista = artista.id
                    {
                        if let tracks = SpotifyRequest.SearchArtistTopTracks(idArtista, country: "CL")
                        {
                            controller.listaCanciones = tracks
                        }
                        
                        if let object = SpotifyRequest.SearchArtistAlbums(idArtista)
                        {
                            let albums = object.items as! Array<AlbumObjectSimple>
                            controller.listaAlbumes = albums
                        }
                    }
                    
                    OperationQueue.main.addOperation(){
                        self.app.isNetworkActivityIndicatorVisible = false
                        self.hud?.hide(animated: true)
                        
                        if controllerCenter is ControllerBusquedaAlbum
                        {
                            (controllerCenter as! ControllerBusquedaAlbum).searchController.isActive = false
                        }
                        else if controllerCenter is ControllerBusquedaArtista
                        {
                            (controllerCenter as! ControllerBusquedaArtista).searchController.isActive = false
                        }

                        navControllerCenter?.pushViewController(controller, animated: true)
                        self.slideMenuController()?.closeLeft()
                    }
                }
            }
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.darkGray
        cell.textLabel?.textColor = UIColor.white
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1
        {
            return self.listaHistorial.count > 0 ? self.listaHistorial.count : 1
        }
        
        return 2
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 1
        {
            return secciones[section]
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return indexPath.section == 1 ? 60 : 40
    }
    
    func agregarAlHistorial(_ object : Serializable)
    {
        
        if self.listaHistorial.count == 10
        {
            self.listaHistorial.removeObject(at: 9)
            self.listaHistorial = NSMutableArray(array: self.listaHistorial)
        }
        
        self.listaHistorial.add(object)
        OperationQueue.main.addOperation(){
            
            self.tableView.reloadData()
        }
        print("AgregarAlHistorial | count: \(listaHistorial.count)")
        
    }

}
