//
//  CellBusqueda.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class CellBusqueda: UITableViewCell {

    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelSubtitulo: UILabel!
    @IBOutlet weak var theImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValores(_ titulo : String?, subtitulo : String?, imagePath : String)
    {
        self.labelTitulo.text = titulo
        self.labelSubtitulo.text = subtitulo
        
        self.backgroundColor = UIColor.darkGray
        
        self.labelTitulo.textColor = UIColor.white
        self.labelSubtitulo.textColor = UIColor.white
        
        if imagePath != ""
        {
            self.theImageView.image = UIImage(contentsOfFile: imagePath)
            self.theImageView.layer.cornerRadius = 30
            self.theImageView.clipsToBounds = true
        }
        
        
    }
    
}
