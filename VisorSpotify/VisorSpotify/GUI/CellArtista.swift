//
//  CellArtista.swift
//  VisorSpotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit

class CellArtista: UITableViewCell {

    @IBOutlet weak var theImageView: UIImageView!
    @IBOutlet weak var labelArtista: UILabel!
    @IBOutlet weak var labelSeguidores: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValores(_ artista : String?, seguidores : String?, imagePath : String)
    {
        self.labelArtista.text = artista
        self.labelSeguidores.text = seguidores
        
        self.backgroundColor = UIColor.darkGray
        
        self.labelArtista.textColor = UIColor.white
        self.labelSeguidores.textColor = UIColor.white
        
        self.theImageView.image = imagePath != "" ? UIImage(contentsOfFile: imagePath) : UIImage(named: "spotify")
        self.theImageView.layer.cornerRadius = 30
        self.theImageView.clipsToBounds = true
        
        
    }
    
}
