//
//  ControllerBusquedaAlbum.swift
//  Visor Spotify
//
//  Created by Bastian Veliz Vega on 25-06-15.
//  Copyright (c) 2015 Bastian Veliz Vega. All rights reserved.
//

import UIKit
import MBProgressHUD

class ControllerBusquedaAlbum: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    var searchController : UISearchController = UISearchController()
    
    var listaElementos : NSMutableArray = NSMutableArray()
    var app = UIApplication.shared
    let hud = MBProgressHUD(for: UIApplication.shared.keyWindow!)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.slideMenuController()?.addLeftGestures()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.searchController = ({
            // Two setups provided below:
            
            // Setup One: This setup present the results in the current view.
            let controller = UISearchController(searchResultsController: nil)
            
            controller.searchResultsUpdater = self
            controller.hidesNavigationBarDuringPresentation = false
            controller.dimsBackgroundDuringPresentation = false
            
            controller.searchBar.searchBarStyle = UISearchBarStyle.prominent
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //UISearchResultsUpdating protocol
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchText = self.searchController.searchBar.text
        let queue = OperationQueue()
        
        if searchText != ""
        {
            self.hud?.label.text = "Buscando"
            self.hud?.show(animated: true)
            self.hud?.label.text = "Buscando"
            self.hud?.show(animated: true)
            app.isNetworkActivityIndicatorVisible = true
            queue.addOperation(){
                
                if let pagingObject = SpotifyRequest.SearchAlbums(searchText!)
                {
                    if pagingObject.items.count > 0{
                        let arregloAlbums = pagingObject.items as! Array<AlbumObjectSimple>
                        self.listaElementos.removeAllObjects()
                        self.listaElementos.addObjects(from: arregloAlbums)
                        
                        for album in arregloAlbums
                        {
                            
                            SpotifyRequest.descargarImagenes(album.images)
                        }
                    }
                    else
                    {
                        self.listaElementos.removeAllObjects()
                    }
                }
                else
                {
                    self.listaElementos.removeAllObjects()
                }
                
                
                //actualizar GUI
                OperationQueue.main.addOperation(){
                    self.tableView.separatorStyle = self.listaElementos.count > 0 ? UITableViewCellSeparatorStyle.singleLine : UITableViewCellSeparatorStyle.none
                    self.tableView.reloadData()
                    self.app.isNetworkActivityIndicatorVisible = false
                    self.hud?.hide(animated: true)
                }
            }
        }
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        OperationQueue.main.addOperation(){
            self.listaElementos.removeAllObjects()
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            self.tableView.reloadData()
            self.app.isNetworkActivityIndicatorVisible = false
            self.hud?.hide(animated: true)
        }
    }
    
    // MARK: - Table view data source
    
    /*override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0
    }*/
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaElementos.count > 0 ? listaElementos.count : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellMensaje = Bundle.main.loadNibNamed("CellMensajeBusqueda", owner: self, options: nil)?[0] as! CellMensajeBusqueda
        
        if listaElementos.count > 0
        {
            let cell = Bundle.main.loadNibNamed("CellBusqueda", owner: self, options: nil)?[0] as! CellBusqueda
            let album = listaElementos[indexPath.row] as! AlbumObjectSimple
            var imagePath = ""
            
            
            if album.images.count > 0
            {
                if let url = album.images.first?.url
                {
                    imagePath = DownloadHelper.getImagePath(url)
                }
                
            }
            
            cell.setValores(album.name, subtitulo: "", imagePath: imagePath)
            return cell
        }
        
        return cellMensaje
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.listaElementos.count > 0
        {
            let queue = OperationQueue()
            let album = listaElementos[indexPath.row] as! AlbumObjectSimple
            let storyBoard = UIStoryboard(name: "StoryboardSpotify", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "ControllerAlbum") as! ControllerAlbum
            
            controller.album = album
            self.hud?.label.text = "Buscando"
            self.hud?.show(animated: true)
            app.isNetworkActivityIndicatorVisible = true
            
            queue.addOperation(){
                
                if let object = SpotifyRequest.SearchAlbumTracks(album.id!)
                {
                    controller.listaCanciones = object.items as! Array<TrackObject>
                }
                
                let navControllerLeft = self.slideMenuController()?.leftViewController as? UINavigationController
                let controllerMenu = navControllerLeft?.viewControllers.last as? ControllerMenu
                
                controllerMenu?.agregarAlHistorial(album)
                
                OperationQueue.main.addOperation(){
                    self.app.isNetworkActivityIndicatorVisible = false
                    self.hud?.hide(animated: true)
                    self.searchController.isActive = false
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
            
            
        }
    }

    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.darkGray
        cell.textLabel?.textColor = UIColor.white
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.listaElementos.count > 0
        {
            return 60
        }
        
        return 190
    }
    
    
    @IBAction func openLeft(_ sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    
}
